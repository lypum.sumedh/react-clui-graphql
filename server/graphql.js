const { ApolloServer, gql } = require('apollo-server-express');
const { toCommand } = require('@replit/clui-gql');
const { introspectionFromSchema } = require('graphql');
const json2md = require('json2md');

const testJson = require("./react_problem.json");

const maharashtra = [];
testJson.india.maharashtra.data.map((resp, inx) => {
  maharashtra.push({ "inx": inx, "data": resp.area });
});

const punjab = [];
testJson.india.punjab.data.map((resp, inx) => {
  punjab.push({ "inx": inx, "data": resp.area });
})

const india = { "maharashtra": maharashtra, "punjab": punjab }

const world = [];
testJson.world.data.map((resp, inx) => {
  world.push({ "inx": inx, "data": resp.area });
})

const mumbai = [
  { "name": "North Mumbai", "count": testJson.india.maharashtra.city["city-data"].Mumbai["North-Mumbai"].places },
  { "name": "North South", "count": testJson.india.maharashtra.city["city-data"].Mumbai["North-south"].places }
]

const typeDefs = gql`
  type CluiSuccessOutput {
    message: String!
  }

  type CluiMarkdownOutput {
    markdown: String!
  }

  type CluiErrorOutput {
    error: String!
  }

  type Clui {
    "India"
    india: India
    "World"
    world: World
    "Mumbai"
    mumbai: Mumbai
  }

  union CluiOutput = CluiSuccessOutput | CluiMarkdownOutput | CluiErrorOutput

  "India"
  type India {
    "Maharashtra"
    maharashtra: CluiOutput
    "Punjab"
    punjab: CluiOutput
  }

  "World"
  type World {
    "Country"
    list: CluiOutput
  }

  "Mumbai"
  type Mumbai {
    "Mumbai"
    list: CluiOutput
  }

  type Query {
    clui: Clui!
    command: String!
  }
`;

const resolvers = {
  Query: {
    clui: () => ({}),

    command: (_, __, ___, { schema }) => {
      const introspection = introspectionFromSchema(schema);

      // Create a command tree from graphql introspection data. This could be done on
      // the server or the client.
      const root = toCommand({
        // 'query' or 'mutation'
        operation: 'query',

        // The name of the graphql type that has the fields that act as top level commands
        rootTypeName: 'Clui',

        // the path at wich the above type appears in the graph
        mountPath: ['clui'],

        // GraphQL introspection data
        introspectionSchema: introspection.__schema,

        // Configure fields and fragments for the output of the GraphQL operation string
        output: () => ({
          fields: '...CluiOutput',
          fragments: `
fragment CluiOutput on CluiOutput {
  ...on CluiSuccessOutput {
    message
  }
  ...on CluiMarkdownOutput {
    markdown
  }
  ...on CluiErrorOutput {
    error
  }
}`
        })
      });

      return JSON.stringify(root);
    }
  },
  Clui: {
    india: () => ({}),
    world: () => ({}),
    mumbai: () => ({})
  },
  India: {
    maharashtra: () => {
      const markdown = json2md([
        {
          table: {
            headers: ['Srno', 'State'],
            rows: india.maharashtra.map((c) => ({ Srno: c.inx, State: c.data }))
          }
        }
      ]);

      return { markdown };
    },
    punjab: () => {
      const markdown = json2md([
        {
          table: {
            headers: ['Srno', 'State'],
            rows: india.punjab.map((c) => ({ Srno: c.inx, State: c.data }))
          }
        }
      ]);

      return { markdown };
    },
  },
  World: {
    list: () => {
      const markdown = json2md([
        {
          table: {
            headers: ['Srno', 'Country'],
            rows: world.map(c => ({ Srno: c.inx, Country: c.data }))
          }
        }
      ]);

      return { markdown };
    },
  },
  Mumbai: {
    list: () => {
      const markdown = json2md([
        {
          table: {
            headers: ['Region', 'Places'],
            rows: mumbai.map(c => ({ Region: c.name, Places: c.count }))
          }
        }
      ]);

      return { markdown };
    },
  },
  CluiOutput: {
    __resolveType(obj) {
      if (obj.error) {
        return 'CluiErrorOutput';
      }

      if (obj.markdown) {
        return 'CluiMarkdownOutput';
      }

      return 'CluiSuccessOutput';
    }
  }
};

module.exports = server => {
  const apollo = new ApolloServer({ typeDefs, resolvers });
  apollo.applyMiddleware({ app: server });
};
